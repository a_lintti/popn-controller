# Popn Controller

Arduino code is a modified version of the code [here](https://github.com/knuckleslee/RhythmCodes/).

The positions of the buttons are based on the dimensions [here](https://github.com/CrazyRedMachine/PopnPanel).


Leds are connected to the even pins and the buttons to the odd pins. Below is a schematic for one button. All of the buttons follow the same format, they are just connected to different pins and the value of the resistor depends on the color of the LED.

![Circuit for one button](circuit.png "Circuit for one button")

I used Cherry MX black switches for the buttons. The switches are mounted to a 3d-printed base (`base_v3.obj`). Between the switches and the 3d-printed hats (`hat_v3.obj`) there is a normal keycap that is glued to the hat. The keycap is required since the connection to the switches must more precise than the 3d-printer I used could produce. 

### The Controller
![](controller.png)
